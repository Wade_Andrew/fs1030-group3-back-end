import express from "express";

import {
  getAllPatients,
  addPatient,
  getOnePatient,
  editPatient,
  addNote,
  deletePatient,
  uploadPhoto,
  registerNewUser,
  userLogin,
  getDoctorName,
  getDoctors,
  getDoctorsPatients,
} from "../controllers/adminControllers.js";

const router = express.Router();

router.get("/", (req, res) => {
  res.send("Hello World");
});

router.get("/patients/get", getAllPatients);
router.post("/addPatient", addPatient);
router.get("/patients/get/:id", getOnePatient);
router.put("/patients/editPatient/:id", editPatient);
router.put("/patients/notes/:id", addNote);
router.delete("/deletePatient/:id", deletePatient);
router.get("/doctors/get/:id", getDoctorName);
router.get("/doctors/get", getDoctors);
router.get("/doctors/patients/get/:id", getDoctorsPatients);
router.post("/login", userLogin);

// * INCOMPLETE ROUTES BELOW - SAVED FOR FUTURE TESTING * //
router.post("/upload", uploadPhoto);
router.post("/register", registerNewUser);

export default router;
