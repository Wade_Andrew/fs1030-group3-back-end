import express from "express";
import cors from "cors";
import mysql2 from "mysql2";
import fileUpload from "express-fileupload";
import dotenv from "dotenv";
import router from "./routes/adminRoutes.js"; // Routes are defined in this file. Logic for posting to database is contained the the adminControllers file //

const app = express();
dotenv.config();

const PORT = process.env.port || 5000;

app.use(express.json());
app.use(cors());
app.use(router); // Use the routes from adminRoutes.js file //
app.use(fileUpload());

const db = mysql2.createConnection({
  host: process.env.host,
  user: process.env.user,
  password: process.env.password,
  database: process.env.database,
});

db.connect((err) => {
  if (err) throw err;
  console.log("Connected to database");
});
global.db = db;

app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  console.error(err.message, err.stack);
  res.status(statusCode).json({ message: err.message });
  return;
});

app.listen(PORT, () => {
  console.log(`Server started at http://localhost:${PORT}`);
});
