// GET all patient data //
export const getAllPatients = (req, res) => {
  const sqlGet = "SELECT * FROM records ORDER BY id DESC";
  db.query(sqlGet, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(result);
    }
  });
};

// POST new patient to database //
export const addPatient = (req, res) => {
  const { medicalID, name, birthdate, address, medications, allergies, doctorID } = req.body;
  const sqlPost =
    "INSERT INTO records (medicalID, name, birthdate, address, medications, allergies, doctorID) VALUES (?,?,?,?,?,?,?)";
  db.query(
    sqlPost,
    [medicalID, name, birthdate, address, medications, allergies, doctorID],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.status(200).send(result);
      }
    }
  );
};

// GET data for one patient //
export const getOnePatient = (req, res) => {
  const id = req.params.id;
  const sqlGet = "SELECT * FROM records WHERE id = ?";
  db.query(sqlGet, id, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(result);
    }
  });
};

// PUT (edit) data for one patient //
export const editPatient = (req, res) => {
  const id = req.params.id;
  const { medicalID, name, birthdate, address, medications, allergies } = req.body;
  const sqlUpdate =
    "UPDATE records SET medicalID = ?, name = ?, birthdate = ?, address = ?, medications = ?, allergies = ? WHERE id = ?";
  db.query(
    sqlUpdate,
    [medicalID, name, birthdate, address, medications, allergies, id],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.status(200).send(result);
      }
    }
  );
};

// PUT (add) note to one patient's record //
export const addNote = (req, res) => {
  const id = req.params.id;
  const { notes } = req.body;
  const sqlUpdate = "UPDATE records SET notes = ? WHERE id = ?";
  db.query(sqlUpdate, [notes, id], (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(result);
    }
  });
};

// DELETE patient from database //
export const deletePatient = (req, res) => {
  const id = req.params.id;
  const sqlDelete = "DELETE FROM records WHERE id = ?";
  db.query(sqlDelete, id, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(204).send(result);
    }
  });
};

// GET doctor name from database //
// TYLER
export const getDoctorName = (req, res) => {
  const id = req.params.id;
  const sqlGet = "SELECT username FROM registered_users WHERE id = ?";
  db.query(sqlGet, id, (err, result) => {
    if (err) {
      console.log("failed");
    } else {
      res.status(200).send(result);
    }
  });
};

// GET doctors patients from database //
// TYLER
export const getDoctorsPatients = (req, res) => {
  const id = req.params.id;
  const sqlGet = "SELECT * FROM records WHERE doctorID = ? ORDER BY id DESC";
  db.query(sqlGet, id, (err, result) => {
    if (err) {
      console.log("err");
    } else {
      res.status(200).send(result);
    }
  });
};

// GET doctors patients from database //
// TYLER
export const getDoctors = (req, res) => {
  const sqlGet = "SELECT * FROM registered_users";
  db.query(sqlGet, (err, result) => {
    console.log(result);
    if (err) {
      console.log("err");
    } else {
      res.status(200).send(result);
    }
  });
};

//POST to database to check for matching username and password//
// TYLER
export const userLogin = (req, res) => {
  const { username, password } = req.body;
  db.query(
    "SELECT * FROM registered_users WHERE username = ? AND password = ?",
    [username, password],
    (err, result) => {
      console.log(result);
      if (err) {
        res.send({ error: err });
      }
      if (result.length > 0) {
        if (username === "admin") {
          return res.status(200).json({ isAdmin: true });
        }
        res.status(200).json({ id: result[0].id });
      } else {
        res.send({ message: "Incorrect username or password" });
      }
    }
  );
};

// // INCOMPLETE CONTROLLERS BELOW - SAVED FOR FUTURE TESTING //
// _________________________________________________________ //

// NOT COMPLETE * POST photo to database * NOT COMPLETE //
export const uploadPhoto = (req, res) => {
  const file = req.files.file;
  let image_name = file.name;
  let fileExtension = file.mimetype.split("/")[1];
  image_name = `${Date.now()}.${fileExtension}`;

  if (req.files === null) {
    return res.status(400).json({ msg: "No file uploaded" });
  }
  file.mv(`../fs1030-group3-front-end/public/uploads/${image_name}`, (err) => {
    if (err) {
      console.error(err);
      return res.status(500).send(err);
    }
    res.json({ fileName: file.name, filePath: `/uploads/${image_name}` });
  });
};

// NOT COMPLETE * POST a new user to a user database * NOT COMPLETE //
export const registerNewUser = (req, res) => {
  const { username, password } = req.body;
  db.query(
    "INSERT INTO registered_users (username, password) VALUES (?,?)",
    [username, password],
    (err, result) => {
      if (err) {
        console.log(err);
      }
    }
  );
};
